from __future__ import absolute_import, print_function, unicode_literals


def flatten(l):
    return [item for sublist in l for item in sublist]
